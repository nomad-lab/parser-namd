# Tests:
### Source of Tests:

The tests are taken from NAMD (version 2.12) examples in [NAMD](http://www.ks.uiuc.edu/Research/namd/utilities/).

### Example Tests:

#### ApoA1 benchmark 
92224 atoms, periodic, PME [NAMD home link](http://www.ks.uiuc.edu/Research/namd/utilities/apoa1/)
#### tiny benchmark 
507 atoms, periodic, PME [NAMD home link](http://www.ks.uiuc.edu/Research/namd/utilities/tiny/)
#### Interactive BPTI benchmark 
882 atoms, small [NAMD home link](http://www.ks.uiuc.edu/Research/namd/utilities/bpti_imd/)
#### ER-GRE benchmark 
36573 atoms, spherical [NAMD home link](http://www.ks.uiuc.edu/Research/namd/utilities/er-gre/)
